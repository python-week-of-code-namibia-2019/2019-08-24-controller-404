# Python Week Of Code, Namibia 2019

## Task for Instructor

1. Open http://localhost:8000/blog/view/ in the web browser.
   
   You will receive a "Post matching query does not exist" error.
2. https://en.wikipedia.org/wiki/List_of_HTTP_status_codes has a list of HTTP status codes that include errors.
   Our server should be compatible with it.
   Replace 

   ```
   post = Post.objects.get(
       title__iexact=title
   )
   ```
  
   with

   ```
   post = get_object_or_404(
       Post,
       title__iexact=title
   )
   ```
3. Open http://localhost:8000/blog/view/ in the web browser.
4. Change

   ```
   DEBUG = True
   ALLOWED_HOSTS = []
   ```

   to

   ```
   DEBUG = False
   ALLOWED_HOSTS = ["*"]
   ```

   in [mysite/settings.py](mysite/settings.py).
5. Create the file [blog/templates/404.html](blog/templates/404.html) to be used as the template for when the user encounter a 404 error.

## Tasks for Learners

1. Add a new field `author` to `Post`.
2. Create a new migration file.
3. Migrate your database.
4. Create a new view to list posts from the same author.

   Your view **must** handle name of authors without post.

## Extra

You can use a try-except block to handle errors.
For example,

```
from django.http import Http404

def blog(request, title):
    try:
        post = Post.objects.get(
            title__iexact=title
        )
    except Post.DoesNotExist:
        raise Http404("Post does not exist.")

    return render(
        request,
        'blog/post.html',
        {
            "post": post,
        }
    )
```